<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->

    </head>
    <body>
        <h3>Profile View | <a href="{{url('/')}}">Home</a></h3>
        <form action="{{url('/search')}}" method="post">
            {{csrf_field()}}
            <input type="text" name="job_id" placeholder="type job id">
        </form>
        <table class="" border="1">
            <tr>
                <th>Name</th>
                <th>Job Id</th>
                <th>Email</th>
                <th>Phone Number</th>
                <th>Action</th>
            </tr>

            @foreach($crud as $data)
            <tr>
                <td>{{ $data->name}}</td>
                <td>{{ $data->job_id}}</td>
                <td>{{ $data->email}}</td>
                <td>{{ $data->phone_number}}</td>
                <td>
                    <form action="{{url('edit')}}" method="post" style="display: inline">
                        {{csrf_field()}}
                        <input type="hidden" name="id" value="{{ $data->id }}"/>
                        <button type="submit" name="btn">Edit</button>
                    </form>
                    <form action="{{url('delete')}}" method="post" style="display: inline">
                        {{csrf_field()}}
                        <input type="hidden" name="id" value="{{ $data->id }}"/>
                        <button type="submit" name="btn">Delete</button>
                    </form>
                </td>

            </tr>
            @endforeach
        </table>
    </body>
</html>
