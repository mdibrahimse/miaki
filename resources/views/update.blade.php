<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->

    </head>
    <body>
        <form action="{{url('/update')}}" method="post">
            {{csrf_field()}}
            <label>Name</label>
            <input type="text" name="name" value="{{$crud->name}}"><br><br>
            <label>Job ID</label>
            <input type="number" name="job_id" value="{{$crud->job_id}}"><br><br>
            <label>Email</label>
            <input type="email" name="email" value="{{$crud->email}}"><br><br>
            <label>Phone Number</label>
            <input type="number" name="phone_number" value="{{$crud->phone_number}}"><br><br>
            <input type="hidden" name="id" value="{{$crud->id}}"><br><br>
            <button>Submit</button>
        </form>

    </body>
</html>
