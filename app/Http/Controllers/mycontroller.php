<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\crud;

class mycontroller extends Controller {

    public function index(Request $request) {
        $crud = new \App\crud();
        $crud->name = $request->name;
        $crud->job_id = $request->job_id;
        $crud->email = $request->email;
        $crud->phone_number = $request->phone_number;
        $crud->save();
        return redirect('/view');
    }

    public function view() {
        $crud = \App\crud::all();
        return view('view', compact('crud'));
    }

    public function edit(Request $request) {
        $crud = \App\crud::find($request->id);
        return view('update', ['crud' => $crud]);
    }

    public function update(Request $request) {
        $crud = \App\crud::find($request->id);
        $crud->name = $request->name;
        $crud->job_id = $request->job_id;
        $crud->email = $request->email;
        $crud->phone_number = $request->phone_number;
        $crud->save();
        return redirect('/view');
    }
    public function delete(Request $request){
        $crud = \App\crud::find($request->id);
        $crud->delete();
        return redirect('/view');
    }
    public function search(Request $request){
        $searchedData = \App\crud::where('job_id', '=', $request->job_id)->get();
        return view('search', ['searchedData' => $searchedData]);
    }

}
