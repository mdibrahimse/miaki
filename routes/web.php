<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('/insert', 'mycontroller@index');
Route::get('/view', 'mycontroller@view');
Route::post('/edit', 'mycontroller@edit');
Route::post('/update', 'mycontroller@update');
Route::post('/delete', 'mycontroller@delete');
Route::post('/search', 'mycontroller@search');




